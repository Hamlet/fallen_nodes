--[[
    Fallen Nodes - Adds dirt, cobble, snow, straw and cactus nodes to
	the	falling_node group. Papyrus will fall too.
	Copyright © 2018, 2019 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Variable
--

local t_NodesList = {
	'darkage:chalk',
	'darkage:basalt_rubble',
	'darkage:cobble_with_plaster',
	'darkage:darkdirt',
	'darkage:gneiss_rubble',
	'darkage:mud',
	'darkage:ors_rubble',
	'darkage:silt',
	'darkage:slate_rubble'
}


--
-- Overrider
--

for i_Element = 1, 9 do
	fallen_nodes.TurnToFalling(t_NodesList[i_Element])
end


--
-- Table flushing for memory saving
--

t_NodesList = nil
