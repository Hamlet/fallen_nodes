# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/).


## [Unreleased]

	- No further feature planned.


## [1.5.0] - 2019-12-17
### Added

	- All of the Minetest Game's v5.1.0 dirt, cobble, snow, and straw nodes.
	- Option to turn off snow blocks' overriding: Options -> All Options -> Mods -> fallen_nodes
	- Global function to allow third party developers to add their own nodes: fallen_nodes.TurnToFalling("mod_name:node_name")

### Modified

	- Code rewritten from scratch.
	
	

## [1.4.2] - 2019-10-23
### Modified

	- License changed to EUPL v1.2
	- mod.conf set to follow MT v5.x specifics.
	- Minor code improvements.


## [1.4.1] - 2018-05-21
### Modified

	- Mod rearranged to follow Minetest-Mods manifesto's guidelines.

### Removed

	- ../doc/


## [1.4.0] - 2018-04-25
### Added

	- Support for "Landscape" v0.1 by BlockMen

### Changed

	- Corrected a typo preventing to load ../darkage.lua

### Removed

	- Development debug info displayed in the console.


## [1.3.2] - 2018-04-20
### Added

	- ../doc/

### Changed

	- Node overriders now use the "for" cycle, where possible, this makes
		easier to read and mantain the code.
